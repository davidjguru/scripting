# Get Yout Clone

## Welcome

Created by David Rodríguez, @davidjguru. November, 2018. All the hugs to davidjguru@gmail.com
Licensed under GPL - GNU General Public License, version 3


## Purpose
Get Your Clone (GYC) It's an executable script by prompt to automate a targeted process of cleaning, cloning and configuration processing for a Drupal-based project between an external repository and a local development environment.

Its actions reproduce a standard working mechanic in Drupal projects: after its execution, it must have installed on its machine (local or remote with Internet access) a Drupal project duly configured and ready to work: installed dependencies, connection to database and configured, database up, integrated configuration, cache cleaned ...and so on, and so on ...

![Get Your Clone executing example](https://gitlab.com/davidjguru/scripting/raw/master/images/davidjguru_drupal_script_getyourclone_one.png "Get Your Clone")


## Assumptions
Usually, the cost of being able to implement automation processes is to standardize and homogenize all the different parts of a project ... names, paths, libraries, tools, dependencies ... everything needs to be compacted to ensure the integrity of all the process. 

In this sense, GYC also requires homogeneity in different parts, as well as assumes that in the environment where it is executed, several tools are already installed:

- Git
- Composer
- Drush [1][2]
- Apache
- MySQL
- PHP

* [1] Switching Drush Versions (English): https://www.lullabot.com/articles/switching-drush-versions
* [2] Installing Drush with Composer (Spanish): https://medium.com/drupal-y-yo/composer-y-drush-en-el-contexto-de-drupal-9883d2cfb007

## Environments
The script has been tested in:

* Ubuntu 17.10 - artful
* Ubuntu 18.04 LTS - bionic
* Red Hat 6.6 - Santiago


This repo it's just a set of scripts for testing and learn to make automatization processing. For internal use only, to young workmates-oriented.
